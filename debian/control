Source: xrayutilities
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Eugen Wintersberger <eugen.wintersberger@gmail.com>,
           Alexandre Marie <alexandre.marie@synchrotron-soleil.fr>,
           Sebastien Delafond <seb@debian.org>,
           Picca Frédéric-Emmanuel <picca@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-numpy3,
               dh-sequence-python3,
               python3-all-dev,
               python3-h5py,
               python3-lmfit,
               python3-matplotlib,
               python3-numpy,
               python3-numpydoc,
               python3-scipy,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme,
               python3-tk,
               python3-unittest2
Build-Depends-Indep: dh-sequence-sphinxdoc <!nodoc>
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/science-team/python-xrayutilities
Vcs-Git: https://salsa.debian.org/science-team/python-xrayutilities.git
Homepage: https://xrayutilities.sourceforge.io

Package: xrayutilities
Depends: python3-xrayutilities, ${misc:Depends}
Architecture: all
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: python3-xrayutilities
Architecture: any
Section: python
Depends: python3-lmfit,
         python3-matplotlib,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: python-xrayutilities-doc
Breaks: xrayutilities (<< 1.6.0-3)
Replaces: xrayutilities (<< 1.6.0-3)
Description: X-rays data reduction and analysis (Python 3)
 xrayutilities is a collection of scripts used to analyze X-rays
 diffraction data. It consists of a Python package and several
 routines coded in C. It especially useful for the reciprocal space
 conversion of diffraction data taken with linear and area detectors.
 .
 This is the Python 3 version of the package.

Package: python-xrayutilities-doc
Architecture: all
Section: doc
Depends: libjs-mathjax, ${misc:Depends}, ${sphinxdoc:Depends}
Description: X-rays data reduction and analysis (documentation)
 xrayutilities is a collection of scripts used to analyze x-ray
 diffraction data. It consists of a Python package and several
 routines coded in C. It especially useful for the reciprocal space
 conversion of diffraction data taken with linear and area detectors.
 .
 This package includes the manual in HTML format.
